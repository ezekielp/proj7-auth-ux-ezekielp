"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import Flask, redirect, url_for, request, render_template, flash, abort, jsonify
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging
import json

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, Form
from wtforms.validators import DataRequired
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from passlib.apps import custom_app_context as pwd_context

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
from is_safe_url import is_safe_url
from flask_wtf.csrf import CSRFProtect
from flask_httpauth import HTTPBasicAuth

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('db', 27017)
# client = MongoClient(host='0.0.0.0', port=8000)
db = client.tododb
db.userdb.drop()  # Clear out users each time application gets run
users = client.userdb

###
# Login Setup
###
login_manager = LoginManager()
login_manager.setup_app(app)
csrf = CSRFProtect(app)
auth = HTTPBasicAuth()


@login_manager.user_loader
def load_user(user_id):
    return User(user_id, user_id, True)


class User(UserMixin):
    def __init__(self, id, name, auth=False):
        self.id = id
        self.is_authenticated = auth
        self.name = name

    def is_active(self):
        return True

    def is_authenticated(self):
        return self.is_authenticated

    def get_id(self):
        return self.id

    def is_anonymous(self):
        return False


@app.route('/login', methods=['GET', 'POST'])
def login():
    # Check the user token and user authentication status
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    # Fresh login
    form = LoginForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            document = db.userdb.find_one({'username': form.username.data})
            if document:
                hVal = str(document['password'])
                if verify_password(form.password.data, hVal):
                    usr = User(form.username.data, form.username.data, auth=True)
                    # Remember me handler
                    if form.remember_me.data:
                        r = True
                    else:
                        r = False

                    newtoken = generate_auth_token(form.username.data, 300)
                    db.userdb.update_one({'username': form.username.data}, {'$set': {'token': newtoken}})

                    login_user(usr, remember=r)

                    next = request.args.get('next')
                    if next and not is_safe_url(next):
                        return abort(400)

                    return flask.redirect(next or url_for('index'))
            return "Invalid username/password combination"
    return render_template('login.html', form=form)


@app.route('/api/register', methods=['POST', 'GET'])
def register():
    """
    Adds a user to the db.userdb database.
    Creates a document with the username and a hashed password.
    """
    form = RegForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            existing_user = db.userdb.find_one({'username': request.form['username']})
            if existing_user is None:
                pw = request.form['password']
                hVal = hash_password(pw)
                u = request.form['username']
                item_doc = {
                    'username': u,
                    'password': hVal,
                    'token': 'dummy'
                }
                db.userdb.insert_one(item_doc)

                response = jsonify({'username': u})
                response.status_code = 201
                response.headers['location'] = url_for('get_user', id=u, _external=True)
                return response

            return 'That username already exists!', 400

    return render_template('register.html', form=form)


def generate_auth_token(id, expiration=300):
    s = Serializer(app.secret_key, expires_in=expiration)
    return s.dumps({'id': id})


def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False  # valid token, but expired
    except BadSignature:
        return False  # invalid token
    return True


@auth.verify_password
def verify_pw(username, password):
    if current_user.is_authenticated:
        userdoc = db.userdb.find_one({'username': current_user.name})
        if userdoc:
            token = userdoc['token']
            return verify_auth_token(token)
    return False


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/api/token")
@login_required
def get_auth_token():
    token = generate_auth_token(current_user.name, 300)
    return jsonify({ 'token': token.decode('ascii'), 'duration': 300 })


@app.route('/api/users/<string:id>')
def get_user(id):
    user = db.userdb.find_one({'username': id})
    if not user:
        abort(400)
    return jsonify({'username': id})


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for("login"))


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Register')


def hash_password(password):
    return pwd_context.encrypt(password)


def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)


###
# Pages
###


class listAllJson(Resource):
    @auth.login_required
    def get(self):
        documents = db.tododb.find().sort([('location', 1)])
        response = []
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            cl = str(document['close'])
            response.append(loc + "km Open Time: " + op)
            response.append(loc + "km Close Time: " + cl)
        return {
            'Control_Points': response
        }


class listOpenJson(Resource):
    @auth.login_required
    def get(self):
        documents = retrieve()
        response = []
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            response.append(loc + "km Open Time: " + op)
        return {
            'Control_Points': response
        }


class listCloseJson(Resource):
    @auth.login_required
    def get(self):
        documents = retrieve()
        response = []
        for document in documents:
            loc = str(document['location'])
            cl = str(document['close'])
            response.append(loc + "km Close Time: " + cl)
        return {
            'Control_Points': response
        }


class listAllCsv(Resource):
    @auth.login_required
    def get(self):
        documents = db.tododb.find().sort([('location', 1)])
        response = "Location,Open,Close;"
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            cl = str(document['close'])
            response += loc + "km,Open Time: " + op + ",Close Time: " + cl + ";"
        response = response.strip(";")
        return response


class listOpenCsv(Resource):
    @auth.login_required
    def get(self):
        documents = retrieve()
        response = "Location,Open;"
        for document in documents:
            loc = str(document['location'])
            op = str(document['open'])
            response += loc + "km,Open Time: " + op + ";"
        response = response.strip(";")
        return response


class listCloseCsv(Resource):
    @auth.login_required
    def get(self):
        documents = retrieve()
        response = "Location,Close;"
        for document in documents:
            loc = str(document['location'])
            cl = str(document['close'])
            response += loc + "km,Close Time: " + cl + ";"
        response = response.strip(";")
        return response


def retrieve():
    """Helper function to retrieve the desired database entries"""
    top = request.args.get('top', 0, type=int)
    if top != 0:
        documents = db.tododb.find().sort([('location', 1)]).limit(top)
    else:
        documents = db.tododb.find().sort([('location', 1)])
    return documents


# Create routes
# Another way, without decorators
api.add_resource(listAllJson, '/listAll')
api.add_resource(listAllJson, '/listAll/json', endpoint='json')
api.add_resource(listCloseJson, '/listCloseOnly')
api.add_resource(listCloseJson, '/listCloseOnly/json', endpoint='/listCloseOny/json')
api.add_resource(listOpenJson, '/listOpenOnly')
api.add_resource(listOpenJson, '/listOpenOnly/json', endpoint='/listOpenOnly/json')
api.add_resource(listAllCsv, '/listAll/csv', endpoint='csv')
api.add_resource(listOpenCsv, '/listOpenOnly/csv', endpoint='/listOpenOnly/csv')
api.add_resource(listCloseCsv, '/listCloseOnly/csv', endpoint='/listCloseOnly/csv')


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('distance', 1000, type=int)
    date = request.args.get('begin_date', 'You messed up', type=str)
    time = request.args.get('begin_time', 'You messed up', type=str)

    #app.logger.debug("dist={}".format(dist))
    #app.logger.debug("date={}".format(date))
    #app.logger.debug("time={}".format(time))
    #app.logger.debug("km={}".format(km))

    combined = date + ' ' + time
    datetime = arrow.get(combined, 'YYYY-MM-DD HH:mm')

    #app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, datetime.isoformat())
    close_time = acp_times.close_time(km, dist, datetime.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_display')
def _display():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.drop()    # Clear out for the next time it gets displayed
    return render_template('display.html', items=items)


@app.route('/_submit')
def _submit():
    # app.logger.debug("Begin submit function")
    # print(request.args.to_dict())
    km = request.args.get('kilometers', 996, type=float)
    o = request.args.get('open_time', "You fucked up", type=str)
    close = request.args.get('close_time', "You fucked up", type=str)
    item_doc = {
        'location': km,
        'open': o,
        'close': close
    }
    db.tododb.insert_one(item_doc)
    # app.logger.debug("End submit function")
    return redirect(url_for("index"))  # Don't do anything to the page on submit (only update the database)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
