<html>
    <head>
        <title>CIS 322 REST-API</title>
    </head>

    <body>
        <h1>Brevet Control Times Consumer App</h1>
        <h4>Select the brevet information you wish to view and the format in which it was stored.</h5>
        <p>Optional: Show only the first X times for this brevet using the textbox</p>
        <p>(Only works for List Open Only or List Close Only. Must be an integer input > 0.)</p>
        <br>
        <p>JSON:</p>
        <form method="post">
            <input type="submit" name=listAll id=listAll value="List All">
            <input type="submit" name=listOpenOnly id=listOpenOnly value="List Open Only">
            <input type="submit" name=listCloseOnly id=listCloseOnly value="List Close Only">
            <input type="text" name="namejson" id="namejson">
        </form>
        <p>CSV:</p>
        <form method="post">
            <input type="submit" name=listAllCSV id=listAllCSV value="List All">
            <input type="submit" name=listOpenOnlyCSV id=listOpenOnlyCSV value="List Open Only">
            <input type="submit" name=listCloseOnlyCSV id=listCloseOnlyCSV value="List Close Only">
            <input type="text" name="namecsv" id="namecsv">
        </form>
        <ul>
            <?php

            // Get the input field and store in $top
            if ( ! empty($_POST['namejson'])){
                $top = $_POST['namejson'];
                $top = intval($top);
            } elseif (! empty($_POST['namecsv'])) {
                $top = $_POST['namecsv'];
                $top = intval($top);
            } else {
                $top = 0;
            }

            //echo $top;

            //Button distributor
            if ($_POST) {
                if (isset($_POST['listAll'])) {
                    listAll();
                } elseif (isset($_POST['listOpenOnly'])) {
                    listOpenOnly();
                } elseif (isset($_POST['listCloseOnly'])) {
                    listCloseOnly();
                } elseif (isset($_POST['listAllCSV'])) {
                    listAllCSV();
                } elseif (isset($_POST['listOpenOnlyCSV'])) {
                    listOpenOnlyCSV();
                } elseif (isset($_POST['listCloseOnlyCSV'])) {
                    listCloseOnlyCSV();
                }
            }

            function listAll() {
                $json = file_get_contents('http://laptop-service/listAll');
                $obj = json_decode($json);
	            $laptops = $obj->Control_Points;
                foreach ($laptops as $l) {
                    echo "<li>$l</li>";
                }
            }

            function listOpenOnly() {
                global $top;
                $json = file_get_contents('http://laptop-service/listOpenOnly');
                $obj = json_decode($json);
	            $laptops = $obj->Control_Points;
	            if ($top != 0) {
                    foreach (array_slice($laptops, 0, $top) as $l) {
                        echo "<li>$l</li>";
                    }
                } else {
                    foreach ($laptops as $l) {
                        echo "<li>$l</li>";
                    }
                }
            }

            function listCloseOnly() {
                global $top;
                $json = file_get_contents('http://laptop-service/listCloseOnly');
                $obj = json_decode($json);
	            $laptops = $obj->Control_Points;
                if ($top != 0) {
                    foreach (array_slice($laptops, 0, $top) as $l) {
                        echo "<li>$l</li>";
                    }
                } else {
                    foreach ($laptops as $l) {
                        echo "<li>$l</li>";
                    }
                }
            }

            function listAllCSV() {
                $CsvString = file_get_contents('http://laptop-service/listAll/csv');
                $CsvString = substr($CsvString, 1, -2);    // Trim the quotes
                $data = explode(";", $CsvString); //parse the string (; delimits line breaks)
                foreach (array_slice($data, 1) as &$row) {      // Don't display the headers
                    $row = explode(",", $row);    //parse the lines
                    echo "<li>$row[0] $row[1]</li>";  // Location Open
                    echo "<li>$row[0] $row[2]</li>";  // Location Close
                }
            }

            function listOpenOnlyCSV() {
                global $top;
                $CsvString = file_get_contents('http://laptop-service/listOpenOnly/csv');
                $CsvString = substr($CsvString, 1, -2);    // Trim the quotes
                $data = explode(";", $CsvString); //parse the string (; delimits line breaks)
                if ($top != 0) {
                    foreach (array_slice($data, 1, $top) as &$row) {      // Don't display the headers
                        $row = explode(",", $row);    //parse the lines
                        echo "<li>$row[0] $row[1]</li>";  // Location Open
                    }
                } else {
                    foreach (array_slice($data, 1) as &$row) {      // Don't display the headers
                        $row = explode(",", $row);    //parse the lines
                        echo "<li>$row[0] $row[1]</li>";  // Location Open
                    }
                }
            }

            function listCloseOnlyCSV() {
                global $top;
                $CsvString = file_get_contents('http://laptop-service/listCloseOnly/csv');
                $CsvString = substr($CsvString, 1, -2);    // Trim the quotes
                $data = explode(";", $CsvString); //parse the string (; delimits line breaks)
                if ($top != 0) {
                    foreach (array_slice($data, 1, $top) as &$row) {      // Don't display the headers
                        $row = explode(",", $row);    //parse the lines
                        echo "<li>$row[0] $row[1]</li>";  // Location Open
                    }
                } else {
                    foreach (array_slice($data, 1) as &$row) {      // Don't display the headers
                        $row = explode(",", $row);    //parse the lines
                        echo "<li>$row[0] $row[1]</li>";  // Location Open
                    }
                }
            }
            ?>
        </ul>
    </body>
</html>
